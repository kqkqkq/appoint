<?php 
/*
 module:		预约管理验证器
 create_time:	2021-07-02 07:09:17
 author:		
 contact:		
*/

namespace app\admin\validate;
use think\validate;

class Appoint extends validate {


	protected $rule = [
		'mobile'=>['regex'=>'/^1[3456789]\d{9}$/'],
	];

	protected $message = [
		'mobile.regex'=>'手机号码格式错误',
	];

	protected $scene  = [
		'add'=>['mobile'],
		'update'=>['mobile'],
	];



}

