<?php
namespace app\home\facade;

use think\Facade;

class Cat extends Facade
{
    protected static function getFacadeClass()
    {
    	return 'app\home\service\CatagoryService';
    }
}