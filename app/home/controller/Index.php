<?php

namespace app\home\controller;
use app\home\model\Appoint;
use app\home\model\AppointTime;
use app\home\model\AppointTotal;
use app\home\model\Member;
use think\facade\Db;
use think\facade\View;
use app\home\service\AppointService;

class Index extends Base
{
	//首页
	public function index(){
        //日期列表
	    $where = [];
	    $date_list = AppointTotal::getList($where,'*',"id asc");
	    foreach($date_list as $key=>$val){
	        $time = time()-86400;
	        if($time>(strtotime($val['appoint_date'].' 00:00:00'))){
	            unset($date_list[$key]);
            }
        }
	    //时间段列表
        $time_list = AppointTime::getList([],'*','id asc');

        View::assign('date_list',$date_list);
        View::assign('time_list',$time_list);
		$default_themes = config('xhadmin.default_themes') ? config('xhadmin.default_themes') : 'index';
		return view($default_themes.'/index');
	}

	public function add(){

        $appoint_date = input('appoint_date','','trim');
        $batch = input('batch',0,'intval');

        $username = input('username','','trim');
        $mobile = input('mobile','','trim');

        if(!$appoint_date) $this->returnRes(0,'请选择日期');
        //超过当晶21：30刚不可以再选择
        if(time()>=(strtotime($appoint_date.' 21:30:00'))){
            $this->returnRes(0,'当日预约时间已过期');
        }

        if(!$batch) $this->returnRes(0,'请输入时间段');
        if(!$username) $this->returnRes(0,'请输入姓名');
        if(!isAllChinese($username)) $this->returnRes(0,'姓名必须是汉字');

        if(!$mobile) $this->returnRes(0,'请输入手机号码');
        if(!checkMobile($mobile)) $this->returnRes(0,'请输入正确的手机号码');

        //判断我是否已经预约
        $find = AppointService::getAppoing($username,$mobile,$appoint_date);

        if($find){
            $this->returnRes(0,'您已完成当日预约，无须再次预约!');
        }

        //用户注册
        $member_id = Member::register($username,$mobile);
        //判断用户预约是否已达上限

        //判断是否在指定时间范围内
        $appoint = Appoint::isAllowTime($appoint_date,$batch,$member_id);
        if(!$appoint['status']){
            $this->returnRes(0,$appoint['msg']);
            return;
        }

        $appoint_time = AppointTime::getValueById($batch,'time_content');
        $data = [
            'member_id'=>$member_id,
            'appoint_date'=>$appoint_date,
            'appoint_time'=>$appoint_time,
            'username'=>$username,
            'mobile'=>$mobile,
            'batch'=>$batch
        ];

        $res = AppointService::addPoint($data);
        if($res){
            //用户预约数加1
            Member::incById($member_id,'appoint_count');
            //当日预约数加1
            AppointTotal::addPointCount($appoint_date,$batch);

            $this->returnRes(1,'预约成功');
        }else{
            $this->returnRes(0,'预约失败');
        }
    }

    public function loginCheck(){
        $default_themes = config('xhadmin.default_themes') ? config('xhadmin.default_themes') : 'index';
        return view($default_themes.'/loginCheck');
    }

    /**
     * 验收登录
     */
    public function doCheclLogin(){
	    $username = input('username','','trim');
        $mobile = input('mobile','','trim');
        if(!$username) $this->returnRes(0,'请输入姓名');
        if(!isAllChinese($username)) $this->returnRes(0,'姓名必须是汉字');

        if(!$mobile) $this->returnRes(0,'请输入手机号码');
        if(!checkMobile($mobile)) $this->returnRes(0,'请输入正确的手机号码');
        $result = Member::checkLogin($username,$mobile);
        $this->returnRes($result['status'],$result['msg'],$result['id_code']);
    }

    /**
     * @return \think\response\View
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取我的预约列表
     */
    public function checkList(){

        $code = input('code','','trim');
        if(!$code) $this->returnRes(0,'参数有误');
        $mobile = Member::getMobileByCode($code);
        if(!$mobile){
            $this->returnRes(0,'参数有误');
        }
        $list = Appoint::getCheckList($mobile);

        View::assign('mobile',$mobile);
        View::assign('list',$list);
        return view('index/checkList');
    }

    /**
     * @return void
     * 操作核销
     */
    public function check(){
        $mobile = input('mobile','','trim');
        $check_id = input('check_id',0,'intval');
        if(!$mobile) $this->returnRes(0,'请输入手机号码');
        if(!checkMobile($mobile)) $this->returnRes(0,'请输入正确的手机号码');
        if(!$check_id) $this->returnRes(0,'参数有误');
        $result = Appoint::check($mobile,$check_id);
        if($result['status']==1) {
            $this->returnRes(1,'核销成功');
        }else{
            $this->returnRes(0,'预约失败:'.$result['msg']);
        }
    }

    /**
     * @param int $status
     * @param string $msg
     */
    private function returnRes($status=0,$msg='',$data=''){
        $result = [
            'status'=>$status,
            'msg'=>$msg,
            'data'=>$data
        ];
        echo json_encode($result);
        exit;
    }
}
