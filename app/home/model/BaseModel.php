<?php

namespace app\home\model;

use think\Model;

class BaseModel extends Model
{
    public static $page;
    public static $limit;

    public static function getById($id,$field='*')
    {
        return self::where(['id'=>$id])->field($field)->find();
    }

    public static function getValueById($id=0,$field=''){
        return self::where(['id'=>$id])->value($field);
    }

    public static function updateById($data, $id)
    {
        if (is_array($id)) {
            $where = $id;
            return self::where($where)->update($data);
        } else {
            $where = [['id', "=", $id]];
            return self::where($where)->update($data);
        }
    }

    public static function incById($id=0,$field='',$step=1){
        $where = [['id', "=", $id]];
        return self::where($where)->inc($field,$step)->update();
    }

    public static function decById($id=0,$field='',$step=1){
        $where = [['id', "=", $id]];
        return self::where($where)->dec($field,$step)->update();
    }

    /**
     * @param array $data
     * @return int|string
     * 插入记录
     */
    public static function add($data=[]){
        return self::insertGetId($data);
    }

    /**
     * @param $where
     * @param string $field
     * @param string $order
     * @return \think\Collection|\think\Paginator
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 分页查询
     */
    public static function getPageList($where, $field = "*", $order = "")
    {
        if (!$order) $order = ['id', "desc"];
        if (!empty(self::$page) && !empty(self::$limit)) {
            return self::where($where)->field($field)->order($order[0], "desc")->paginate(self::$limit, false, self::$page);
        } else {
            return self::where($where)->field($field)->order($order[0], "desc")->select();
        }
    }

    /**
     * @param $where
     * @param string $field
     * @param string $order
     * @return \think\Collection|\think\Paginator
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 分页查询
     */
    public static function getList($where=[], $field = "*", $order = "")
    {
        if (!$order) $order = "id desc";
        return self::where($where)->field($field)->order($order)->select();
    }

    public static function deleteById($id=0){
        $where = [['id', "=", $id]];
        return self::where($where)->delete();
    }

    public static function deleteByMemberId($member_id=0){
        $where =['member_id'=>$member_id];
        return self::where($where)->delete();
    }

    /**
     * @param $page
     * @param $limit
     * 设置分页
     */
    public static function setPage($page, $limit)
    {
        self::$page = $page;
        self::$limit = $limit;
    }
}