<?php 
/*
 module:		预约管理模型
 create_time:	2021-07-01 15:06:29
 author:		
 contact:		
*/

namespace app\home\model;
use think\Model;

class AppointTotal extends BaseModel {

	protected $pk = 'id';
 	protected $name = 'appoint_total';

    /**
     * @param string $appoint_date
     * @param int $batch
     * @return bool|void
     * 预约数加1
     */
 	public static function addPointCount($appoint_date='',$batch=0){
 	    if(!$appoint_date || !$batch) return;
 	    if($batch<=0 || $batch>3) return;
 	    if($batch==1) $field = 'level_one_point';
 	    if($batch==2) $field = 'level_two_point';
 	    if($batch==3) $field = 'level_three_point';
 	    $where = ['appoint_date'=>$appoint_date];
        self::where($where)->inc($field,1)->update();
        return true;
    }

    /**
     * @param string $appoint_date
     * @param int $batch
     * @return bool|void
     * 核销数加1
     */
    public static function addCheckCount($appoint_date='',$batch=0){
        if(!$appoint_date || !$batch) return;
        if($batch<=0 || $batch>3) return;
        if($batch==1) $field = 'level_one_check';
        if($batch==2) $field = 'level_two_check';
        if($batch==3) $field = 'level_three_check';
        $where = ['appoint_date'=>$appoint_date];
        self::where($where)->inc($field,1)->update();
        return true;
    }

}

