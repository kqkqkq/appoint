<?php 
/*
 module:		预约管理模型
 create_time:	2021-07-01 15:06:29
 author:		
 contact:		
*/

namespace app\home\model;
use think\Model;

class AppointTime extends BaseModel {

	protected $pk = 'id';
 	protected $name = 'appoint_time';

    /**
     * @return mixed|string
     * 获取最开始的时间
     */
 	public static function getLevelOneStart(){
 	    $res = self::getValueById(1,'time_content');
 	    $time = explode('-',$res);
 	    return $time[0];
    }

    /**
     * @return mixed|string
     * 获取最后的结束时间
     */
    public static function getLevelThreeEnd(){
        $res = self::getValueById(3,'time_content');
        $time = explode('-',$res);
        return $time[1];
    }

}

