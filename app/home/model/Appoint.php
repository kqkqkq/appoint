<?php 
/*
 module:		预约管理模型
 create_time:	2021-07-01 15:06:29
 author:		
 contact:		
*/

namespace app\home\model;
use think\Model;

class Appoint extends BaseModel {

	protected $pk = 'id';
 	protected $name = 'appoint';

    /**
     * @return array
     * 判断是否在指定时间范围内
     * 1月20号到2月25号
     * 晚上6：30到7：30 7：30到8：30，8：30到9：30 每个时间段200人，一共600人
     */
    public static function isAllowTime($appoint_date='',$batch=0,$member_id=0): array
    {
        $member_point_count = self::where(['member_id'=>$member_id])->count();
        if($member_point_count>=35){
            return ['status'=>0,'msg'=>'您的预约已达上限'];
        }
        //一天600个
        $all= self::where(['appoint_date'=>$appoint_date])->count();
        if($all>=600){
            return ['status'=>0,'msg'=>'当日段报名已满'];
        }

        //判断当前时间段是否可以报名
        $find = AppointTime::getById($batch);

        //未开始
        $appoint_time = explode('-',$find['time_content']);
        $start = strtotime($appoint_date.' '.$appoint_time[0].':00');
        $end = strtotime($appoint_date.' '.$appoint_time[1].':00');
        //未开始
//        if(time()<$start){
//            return ['status'=>0,'msg'=>'当日时间段报名未开始'];
//        }
        //已过期
        if(time()>$end){
            return ['status'=>0,'msg'=>'当日时间段报名已结束'];
        }
        //判断 每个时间段是否报名已满
        $where = [
            'appoint_date'=>$appoint_date,
            'batch'=>$batch
        ];
        $count = self::where($where)->count();
        if($count>=200){
            return ['status'=>0,'msg'=>'当前时间段报名已满'];
        }
        return ['status'=>1,'msg'=>'可报名'];
    }

    /**
     * @return array
     * 判断是否在指定时间范围内
     * 1月20号到2月25号
     * 晚上6：30到7：30 7：30到8：30，8：30到9：30 每个时间段200人，一共600人
     */
    public static function isAllowTime2(): array
    {
        $date = date("Y-m-d");

        $dateStart1 = $date.' 18:30:00';
        $dateEnd1 = $date.' 19:30:00';

        $dateStart2 = $date.' 19:30:01';
        $dateEnd2 = $date.' 20:30:00';

        $dateStart3 = $date.' 20:30:01';
        $dateEnd3 = $date.' 21:30:00';

        $dateStartTime1 = strtotime($dateStart1);
        $dateEndTime1 = strtotime($dateEnd1);
        $dateStartTime2 = strtotime($dateStart2);
        $dateEndTime2 = strtotime($dateEnd2);
        $dateStartTime3 = strtotime($dateStart3);
        $dateEndTime3 = strtotime($dateEnd3);

        $time = time();
        //判断是否可以开始了
        if($time<$dateStartTime1){
            return ['status'=>0,'msg'=>'活动还未开始'];
        }
        if($time>$dateEndTime3){
            return ['status'=>0,'msg'=>'活动已结束'];
        }

        $batch = 0;
        if($time>=$dateStartTime1 && $time<=$dateEndTime1){
            $batch = 1;
        }
        if($time>=$dateStartTime2 && $time<=$dateEndTime2){
            $batch = 2;
        }
        if($time>=$dateStartTime3 && $time<=$dateEndTime3){
            $batch = 3;
        }

        //判断 每个时间段是否报名已满
        $where = [
            'appoint_date'=>$date,
            'batch'=>$batch
        ];
        $count = self::where($where)->count();
        if($count>=200){
            return ['status'=>0,'msg'=>'当前时间段报名已满'];
        }
        return ['status'=>1,'msg'=>'可报名'];
    }

    /**
     * @return array
     * 判断是否在指定时间范围内
     * 1月20号到2月25号
     * 晚上6：30到7：30 7：30到8：30，8：30到9：30 每个时间段200人，一共600人
     */
    public static function getCheckList($mobile='')
    {
//        $date = date("Y-m-d");
//        $dateStart1 = $date.' 18:30:00';
//        $dateEnd3 = $date.' 21:30:00';
//        $dateStartTime1 = strtotime($dateStart1);
//        $dateEndTime3 = strtotime($dateEnd3);
//        $time = time();
//        //判断是否可以开始了
//        if($time<$dateStartTime1){
//            return ['status'=>0,'msg'=>'活动还未开始'];
//        }
//        if($time>$dateEndTime3){
//            return ['status'=>0,'msg'=>'活动已结束'];
//        }
        //判断 每个时间段是否报名已满
        $where = [
            'mobile'=>$mobile
        ];
        $list = self::where($where)->order("check_time asc,appoint_date asc")->select();
        if($list){
            foreach($list as $key=>&$val){
                $val['check_status'] = $val['check_time']>0?'已核销':'未核销';
                $val['create_time'] = date("Y-m-d H:i:s",$val['create_time']);
                $val['appoint_time_title'] = AppointTime::getValueById($val['batch'],'time_title');
                $appoint_date = $val['appoint_date'];
                //未开始
                $appoint_time = explode('-',$val['appoint_time']);
                $start = strtotime($appoint_date.' '.$appoint_time[0].':00');
                $end = strtotime($appoint_date.' '.$appoint_time[1].':00');
                //未开始
                if(time()<$start){
                    $state = 1;
                }
                //已过期
                if(time()>$end){
                    $state = 2;
                }
                $val['state'] = $state;
            }
        }else{
            $list = [];
        }
        return $list;
    }

    /**
     * @param $mobile
     * @param $id
     * @return array
     * 核销操作
     */
    public static function check($mobile='',$id=0){
        $find = self::getById($id);
        if($find['mobile']!=$mobile){
            return ['status'=>0,'msg'=>'手机号码有误'];
        }
        if($find['check_time']>0){
            return ['status'=>0,'msg'=>'已核销，无须再次核销'];
        }

        $appoint_date = $find['appoint_date'];
        $levelOne = AppointTime::getLevelOneStart();
        if(time()<strtotime($appoint_date.' '.$levelOne.':00')){
            return ['status'=>0,'msg'=>'今日核销还未开始'];
        }

        $levelThree = AppointTime::getLevelThreeEnd();
        if(time()>strtotime($appoint_date.' '.$levelThree.':00')){
            return ['status'=>0,'msg'=>'今日核销已结束'];
        }

        //判断今天是否已经核销了600
        $all = self::where(['appoint_date'=>$appoint_date])->count();
        if($all>=600){
            return ['status'=>0,'msg'=>'今日核销已达上限'];
        }

        //判断当前时间段是否已经核销了200
        $batch = $find['batch'];
        $count = self::where(['appoint_date'=>$appoint_date,'batch'=>$batch])->count();
        if($count>=200){
            return ['status'=>0,'msg'=>'今日当前时间段核销已达上限'];
        }

        //判断是否到了当日核销时间段
        $appoint_time = explode('-',$find['appoint_time']);
        $start = strtotime($appoint_date.' '.$appoint_time[0].':00');
        $end = strtotime($appoint_date.' '.$appoint_time[1].':00');

        if(time()<$start){
            return ['status'=>0,'msg'=>'今日当前时段核销未开始'];
        }
        if(time()>$end){
            return ['status'=>0,'msg'=>'今日当前时段核销已结束'];
        }

        if(time()>=$start && time()<=$end ){
            self::updateById(['check_time'=>time()],$id);
            //当日核销数加1
            AppointTotal::addCheckCount($appoint_date,$find['batch']);
            return ['status'=>1,'msg'=>'核销成功'];
        }else{
            return ['status'=>0,'msg'=>'系统异常:请联系工作人员'];
        }
    }

    /**
     * @param $mobile
     * @param $id
     * @return array
     * 核销操作
     */
    public static function check2($mobile='',$id=0){
        $find = self::getById($id);
        if($find['mobile']!=$mobile){
            return ['status'=>0,'msg'=>'手机号码有误'];
        }
        if($find['check_time']>0){
            return ['status'=>0,'msg'=>'已核销，无须再次核销'];
        }
        self::updateById(['check_time'=>time()],$id);
        AppointTotal::addCheckCount($find['appoint_date'],$find['batch']);
        return ['status'=>1,'msg'=>'核销成功'];
    }
}

