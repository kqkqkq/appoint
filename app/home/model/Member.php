<?php 
/*
 module:		预约管理模型
 create_time:	2021-07-01 15:06:29
 author:		
 contact:		
*/

namespace app\home\model;
use think\Model;

/**
 * Class Member
 * @package app\home\model
 * 用户model
 */
class Member extends BaseModel {

	protected $pk = 'id';
 	protected $name = 'member';

    /**
     * @param string $username
     * @param string $mobile
     * @return int|mixed|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 注册用户
     */
    public static function register($username='',$mobile=''){
        $where = [
            'username'=>$username,
            'mobile'=>$mobile
        ];
        $find = self::where($where)->find();

        if(!$find){
            $id_code = md5($username.$mobile.REG_KEY);
            $data = [
                'username'=>$username,
                'mobile'=>$mobile,
                'id_code'=>$id_code,
                'create_time'=>time()
            ];
            $member_id = self::add($data);
        }else{
            $member_id = $find['id'];
        }
        return $member_id;
    }

    /**
     * @param string $username
     * @param string $mobile
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 核销前登录
     */
    public static function checkLogin($username='',$mobile=''){
        $where = [
            'username'=>$username,
            'mobile'=>$mobile
        ];
        $find = self::where($where)->find();
        if(!$find){
            return ['status'=>0,'msg'=>'您还未预约,请预约后再核销','id_code'=>''];
        }
        return ['status'=>1,'id_code'=>$find['id_code'],'msg'=>'success'];
    }

    /**
     * @param string $code
     * @return mixed|string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     * 获取手机号码
     */
    public static function getMobileByCode($code=''){
        $where = [
            'id_code'=>$code
        ];
        $find = self::where($where)->find();
        if($find){
            return $find['mobile'];
        }
        return '';
    }
}

