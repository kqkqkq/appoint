<?php 
/*
 module:		预约管理
 create_time:	2021-07-01 15:06:29
 author:		
 contact:		
*/

namespace app\home\service;
use app\home\model\Appoint;
use think\exception\ValidateException;
use xhadmin\CommonService;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class AppointService extends CommonService {

    public static function getAppoing($username='',$mobile='',$appoint_date){
        //$date = date('Y-m-d',time());
        $where = [
            ['mobile','=',$mobile],
            ['appoint_date','=',$appoint_date],
        ];
        $res = Appoint::where($where)->find();
        return $res?$res:null;
    }

	/*
 	* @Description  添加
 	*/
	public static function addPoint($data){
		try{
            $data['create_time'] = time();
			$res = Appoint::add($data);
		}catch(ValidateException $e){
			throw new ValidateException ($e->getError());
		}catch(\Exception $e){
			abort(config('my.error_log_code'),$e->getMessage());
		}
		if(!$res){
			throw new ValidateException ('操作失败');
		}
		return $res;
	}
}

