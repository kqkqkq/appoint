/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : sign.tpbysj.cn

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 19/01/2024 14:07:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cd_appoint
-- ----------------------------
DROP TABLE IF EXISTS `cd_appoint`;
CREATE TABLE `cd_appoint`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `appoint_date` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '参观日期',
  `username` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号码',
  `batch` tinyint(1) NULL DEFAULT 0 COMMENT '批次',
  `check_time` int(10) NULL DEFAULT 0 COMMENT '核销时间',
  `create_time` int(11) NULL DEFAULT NULL COMMENT '预约时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `appoint_date`(`appoint_date`) USING BTREE,
  INDEX `mobile`(`mobile`) USING BTREE,
  INDEX `batch`(`batch`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 108 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cd_appoint
-- ----------------------------
INSERT INTO `cd_appoint` VALUES (1, '2024-01-19', '孔旗', '15640297305', 0, 0, 1705642293);
INSERT INTO `cd_appoint` VALUES (2, '2024-01-19', '孔旗', '15640297305', 0, 0, 1705642307);
INSERT INTO `cd_appoint` VALUES (3, '2024-01-19', '孔旗', '15640297305', 0, 0, 1705642309);

SET FOREIGN_KEY_CHECKS = 1;
